import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-button-input',
    templateUrl: './button-input.component.html',
})
export class ButtonInputComponent {
    @Input() type = 'text';
    @Input() placeholder: string;

    @Output() enter = new EventEmitter<string>();

    control = new FormControl();
    showInput = false;

    submit(value: string) {
        this.enter.emit(value);
        this.showInput = false;
    }


    emitValue() {
        this.enter.emit(this.control.value);
        this.closeInput();
    }

    closeInput() {
        this.control.reset();
        this.showInput = false;
    }
}
