import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
})
export class ButtonComponent {
    @Input() expand = 'block';
    @Input() fill = 'clear';
    @Input() size = 'default';
}
