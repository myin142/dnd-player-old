import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
})
export class InputComponent {
    @Input() type: string;
    @Input() label: string;
    @Input() control: FormControl;
    @Input() lines: string;
}
