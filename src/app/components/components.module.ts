import { NgModule } from '@angular/core';
import { InputComponent } from './input/input.component';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { StepNumberComponent } from './step-number/step-number.component';
import { ButtonComponent } from './button/button.component';
import { CommonModule } from '@angular/common';
import { ButtonInputComponent } from './button-input/button-input.component';
import { TextareaAutosizeDirective } from './directive/textarea-autosize.directive';

@NgModule({
    declarations: [
        InputComponent,
        StepNumberComponent,
        ButtonComponent,
        ButtonInputComponent,
        TextareaAutosizeDirective,
    ],
    exports: [
        InputComponent,
        StepNumberComponent,
        ButtonComponent,
        ButtonInputComponent,
        TextareaAutosizeDirective,
    ],
    imports: [
        IonicModule,
        ReactiveFormsModule,
        CommonModule,
    ],
})
export class ComponentsModule {

}
