import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';

@Component({
    selector: 'app-step-number',
    templateUrl: './step-number.component.html',
})
export class StepNumberComponent {

    @Input() label: string;
    @Input() control: FormControl;
    @Input() setButton = 'Set';
    @Input() addButton = 'Add';
    @Input() subButton = 'Subtract';

    constructor(private popoverCtrl: PopoverController,
                private alertCtrl: AlertController) {
    }

    get value(): number {
        return this.control.value || 0;
    }

    setValue(value: number): void {
        if (isNaN(value)) {
            return;
        }

        this.control.setValue(value);
    }

    async alertInput(): Promise<void> {
        const alert = await this.alertCtrl.create({
            header: 'Change Value',
            inputs: [
                {
                    type: 'number',
                    placeholder: 'Value',
                },
            ],
            buttons: [
                {
                    text: this.addButton,
                    handler: x => this.setValue(this.value + parseFloat(x[0])),
                },
                {
                    text: this.setButton,
                    handler: x => this.setValue(parseFloat(x[0])),
                },
                {
                    text: this.subButton,
                    handler: x => this.setValue(this.value - parseFloat(x[0])),
                },
            ],
        });

        await alert.present();
    }

}
