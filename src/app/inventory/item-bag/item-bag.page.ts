import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ItemBagService } from './item-bag.service';

@Component({
    selector: 'app-item-bag',
    templateUrl: './item-bag.page.html',
})
export class ItemBagPage implements OnInit {

    formGroup: FormGroup;
    private init = false;

    constructor(private builder: FormBuilder,
                private itemBagService: ItemBagService) {
    }

    ngOnInit(): void {
        this.formGroup = this.builder.group({
            copper: [],
            silver: [],
            electrum: [],
            gold: [],
            platinum: [],
            items: [[]],
        });

        this.itemBagService.getItems().subscribe(items => {
            if (!this.init) {
                this.formGroup.setValue(items);
                this.formGroup.valueChanges.subscribe(values => {
                    this.itemBagService.updateItems(values);
                });
                this.init = true;
            }
        });
    }

    addItem(item: string): void {
        const value = this.itemControl.value;
        value.push(item);
        this.itemControl.setValue(value);
    }

    removeItem(index: number): void {
        const value = this.itemControl.value as [];
        value.splice(index, 1);
        this.itemControl.setValue(value);
    }

    get itemControl(): AbstractControl {
        return this.formGroup.get('items');
    }

}
