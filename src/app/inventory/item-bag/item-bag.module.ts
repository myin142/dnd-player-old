import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemBagPage } from './item-bag.page';
import { MenuModule } from '../../menu/menu.module';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../../components/components.module';
import { ItemBagService } from './item-bag.service';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        ItemBagPage,
    ],
    imports: [
        RouterModule.forChild([
            {path: '', component: ItemBagPage},
        ]),
        CommonModule,
        MenuModule,
        IonicModule,
        ComponentsModule,
    ],
    providers: [
        ItemBagService,
    ],
})
export class ItemBagModule {
}
