export interface ItemBag {
    copper: number;
    silver: number;
    electrum: number;
    gold: number;
    platinum: number;
    items: string[];
}
