import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from '../../services/storage.service';
import { StorageKey } from '../../shared/storage-key';
import { filter } from 'rxjs/operators';
import { ItemBag } from './item-bag.model';

@Injectable()
export class ItemBagService {

    itemBag = new BehaviorSubject<ItemBag>(null);

    constructor(private storage: StorageService) {
        this.storage.get<ItemBag>(StorageKey.ITEM_BAG).subscribe(stats => {
            this.itemBag.next(this.mapUndefinedToDefault(stats || {}));

            this.itemBag.subscribe(x => {
                this.storage.save(StorageKey.ITEM_BAG, x).subscribe();
            });
        });
    }

    private mapUndefinedToDefault(stats: Partial<ItemBag>): ItemBag {
        return {
            copper: stats.copper || 0,
            silver: stats.silver || 0,
            electrum: stats.electrum || 0,
            gold: stats.gold || 0,
            platinum: stats.platinum || 0,
            items: stats.items || [],
        };
    }

    updateItems(items: Partial<ItemBag>): void {
        this.itemBag.next({
            ...this.itemBag.value,
            ...items,
        });
    }

    getItems(): Observable<ItemBag> {
        return this.itemBag.pipe(
            filter(x => x != null),
        );
    }
}
