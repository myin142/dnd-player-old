import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryPage } from './inventory.page';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        InventoryPage,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: InventoryPage,
                children: [
                    {path: '', redirectTo: 'items', pathMatch: 'full'},
                    {path: 'items', loadChildren: () => import('./item-bag/item-bag.module').then(x => x.ItemBagModule)},
                ],
            },
        ]),
        IonicModule,
    ],
})
export class InventoryModule {
}
