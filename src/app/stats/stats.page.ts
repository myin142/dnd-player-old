import { Component } from '@angular/core';
import { StatsService } from '../services/stats.service';
import { ModalController } from '@ionic/angular';
import { SetBaseStatsComponent } from './ability-stats/set-base-stats/set-base-stats.component';

@Component({
    selector: 'app-stats',
    templateUrl: './stats.page.html',
})
export class StatsPage {

    constructor(public stats: StatsService,
                private modal: ModalController) {
    }

    async openSetStats(): Promise<void> {
        const modalRef: HTMLIonModalElement = await this.modal.create({
            component: SetBaseStatsComponent,
        });

        await modalRef.present();
    }

}
