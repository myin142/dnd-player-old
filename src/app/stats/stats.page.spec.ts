import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsPage } from './stats.component';

describe('StatsComponent', () => {
    let component: StatsPage;
    let fixture: ComponentFixture<StatsPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [StatsPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatsPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

/*
    it('should create', () => {
        expect(component).toBeTruthy();
    });
*/
});
