import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsPage } from './stats.page';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuModule } from '../menu/menu.module';

@NgModule({
    declarations: [
        StatsPage,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: StatsPage,
                children: [
                    {path: '', redirectTo: 'ability', pathMatch: 'full'},
                    {path: 'ability', loadChildren: () => import('./ability-stats/ability-stats.module').then(x => x.AbilityStatsModule)},
                    {path: 'player', loadChildren: () => import('./player-stats/player-stats.module').then(x => x.PlayerStatsModule)},
                    {path: 'skills', loadChildren: () => import('./skills/skills.module').then(x => x.SkillsModule)},
                ],
            },
        ]),
        IonicModule,
        MenuModule,
    ],
})
export class StatsModule {
}
