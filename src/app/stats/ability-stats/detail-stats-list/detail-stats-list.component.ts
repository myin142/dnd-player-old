import { Component, OnInit } from '@angular/core';
import { detailStats, Stat, StatModifier } from '../../../shared/stats.model';
import { StatsService } from '../../../services/stats.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatItem } from '../stats-list/stats-list.component';

@Component({
    selector: 'app-detail-stats-list',
    templateUrl: './detail-stats-list.component.html',
})
export class DetailStatsListComponent implements OnInit {

    private detailStats: Stat[];
    statList: Observable<StatItem[]>;

    constructor(private statsService: StatsService) {
    }

    ngOnInit(): void {
        this.detailStats = detailStats.map(x => {
            x.name = `${x.name} (${groupAbbreviationMap[x.statGroup]})`;
            return x;
        });

        this.statList = this.statsService.getStatsModifier().pipe(
            map(modifier => {
                return this.detailStats.map((stat, index) => this.toStatItem(stat, modifier, index));
            }),
        );
    }

    private toStatItem(stat: Stat, statMod: StatModifier, index: number): StatItem {
        const statItem = {
            name: stat.name,
            value: statMod.statModifier[stat.statGroup],
            selected: statMod.detailProficiency.indexOf(index) !== -1,
        };

        if (statItem.selected) {
            statItem.value += statMod.proficiency;
        }

        return statItem;
    }

    addProficiency(index: number): void {
        this.statsService.addDetailProficiency(index);
    }
}

const groupAbbreviationMap = [
    'str',
    'dex',
    'con',
    'int',
    'wis',
    'cha',
];
