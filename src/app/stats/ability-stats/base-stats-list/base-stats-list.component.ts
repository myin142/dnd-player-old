import { Component, OnInit } from '@angular/core';
import { StatsService } from '../../../services/stats.service';
import { BaseStats, baseStats, Stat, StatGroup, StatModifier } from '../../../shared/stats.model';
import { Observable } from 'rxjs';
import { map, pluck, tap } from 'rxjs/operators';
import { StatItem } from '../stats-list/stats-list.component';

@Component({
    selector: 'app-base-stats-list',
    templateUrl: './base-stats-list.component.html',
})
export class BaseStatsListComponent implements OnInit {

    statsList: Observable<StatItem[]>;
    private baseStats = baseStats;

    constructor(private statsService: StatsService) {
    }

    ngOnInit(): void {
        this.statsList = this.statsService.getStatsModifier().pipe(
            map(modifier => {
                return this.baseStats.map((stat, index) => this.toStatItem(stat, modifier, index));
            }),
        );
    }

    private toStatItem(stat: Stat, statMod: StatModifier, index: number): StatItem {
        const statItem = {
            name: stat.name,
            value: statMod.statModifier[stat.statGroup],
            selected: statMod.baseProficiency.indexOf(index) !== -1,
        };

        if (statItem.selected) {
            statItem.value += statMod.proficiency;
        }

        return statItem;
    }

    addProficiency(index: number): void {
        this.statsService.addBaseProficiency(index);
    }

}
