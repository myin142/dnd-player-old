import { StatsService } from '../../services/stats.service';
import { ModalController } from '@ionic/angular';
import { SetBaseStatsComponent } from './set-base-stats/set-base-stats.component';
import { Component } from '@angular/core';

@Component({
    selector: 'app-ability-stats',
    templateUrl: './ability-stats.page.html',
})
export class AbilityStatsPage {

    constructor(public stats: StatsService,
                private modal: ModalController) {
    }

    async openSetStats(): Promise<void> {
        const modalRef: HTMLIonModalElement = await this.modal.create({
            component: SetBaseStatsComponent,
        });

        await modalRef.present();
    }
}
