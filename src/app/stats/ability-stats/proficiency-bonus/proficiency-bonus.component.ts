import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, filter } from 'rxjs/operators';
import { StatsService } from '../../../services/stats.service';

@Component({
    selector: 'app-proficiency-bonus',
    templateUrl: './proficiency-bonus.component.html',
})
export class ProficiencyBonusComponent implements OnInit {
    control = new FormControl(0);

    constructor(private stats: StatsService) {
    }

    ngOnInit(): void {
        this.stats.getProficiency().pipe(
            filter(x => this.control.value !== x),
        ).subscribe(x => {
            this.control.setValue(x);
        });

        this.control.valueChanges.pipe(
            debounceTime(500),
        ).subscribe(value => {
            this.setProficiencyBonus(value);
        });
    }

    private setProficiencyBonus(bonus: number): void {
        this.stats.setProficiencyBonus(bonus);
    }

}
