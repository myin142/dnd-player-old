import { NgModule } from '@angular/core';
import { SetBaseStatsComponent } from './set-base-stats/set-base-stats.component';
import { BaseStatsFormComponent } from './base-stats-form/base-stats-form.component';
import { DetailStatsListComponent } from './detail-stats-list/detail-stats-list.component';
import { BaseStatsListComponent } from './base-stats-list/base-stats-list.component';
import { StatsListComponent } from './stats-list/stats-list.component';
import { ProficiencyBonusComponent } from './proficiency-bonus/proficiency-bonus.component';
import { PassivePerceptionComponent } from './passive-perception/passive-perception.component';
import { MenuModule } from '../../menu/menu.module';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../../components/components.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AbilityStatsPage } from './ability-stats.page';

@NgModule({
    imports: [
        CommonModule,
        MenuModule,
        IonicModule,
        ReactiveFormsModule,
        ComponentsModule,
        RouterModule.forChild([
            {path: '', component: AbilityStatsPage},
        ])
    ],
    declarations: [
        AbilityStatsPage,
        SetBaseStatsComponent,
        BaseStatsFormComponent,
        DetailStatsListComponent,
        BaseStatsListComponent,
        StatsListComponent,
        ProficiencyBonusComponent,
        PassivePerceptionComponent,
    ],
    entryComponents: [
        SetBaseStatsComponent,
    ],
})
export class AbilityStatsModule {

}
