import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-stats-list',
    templateUrl: 'stats-list.component.html',
})
export class StatsListComponent {
    @Input() stats: Observable<StatItem[]>;
    @Output() selectClick = new EventEmitter<number>();

    selectIcon(proficient: boolean) {
        return (proficient) ? 'radio-button-on' : 'radio-button-off';
    }
}

export interface StatItem {
    name: string;
    value: number;
    selected: boolean;
}
