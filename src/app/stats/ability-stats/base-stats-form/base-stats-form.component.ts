import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseStats } from '../../../shared/stats.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-base-stats-form',
    templateUrl: './base-stats-form.component.html',
})
export class BaseStatsFormComponent implements OnInit {

    @Input() data: Partial<BaseStats>;
    @Output() submitted = new EventEmitter<BaseStats>();

    formGroup: FormGroup;

    constructor(private builder: FormBuilder) {
    }

    ngOnInit(): void {
        const baseStats = this.data || {};

        this.formGroup = this.builder.group({
            strength: [baseStats.strength, Validators.required],
            dexterity: [baseStats.dexterity, Validators.required],
            constitution: [baseStats.constitution, Validators.required],
            intelligence: [baseStats.intelligence, Validators.required],
            wisdom: [baseStats.wisdom, Validators.required],
            charisma: [baseStats.charisma, Validators.required],
        });
    }

    get formControlKeys(): string[] {
        return Object.keys(this.formGroup.controls);
    }

    async submit(): Promise<void> {
        if (this.formGroup.valid) {
            this.submitted.emit(this.formGroup.value);
        }
    }

}
