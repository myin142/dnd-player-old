import { Component, OnInit } from '@angular/core';
import { StatsService } from '../../../services/stats.service';
import { StatGroup, StatModifier } from '../../../shared/stats.model';

@Component({
    selector: 'app-passive-perception',
    templateUrl: './passive-perception.component.html',
})
export class PassivePerceptionComponent implements OnInit {

    private statMod: StatModifier;

    constructor(private statsService: StatsService) {
    }

    ngOnInit(): void {
        this.statsService.getStatsModifier().subscribe(stat => {
            this.statMod = stat;
        });
    }

    // 10 + Wisdom Mod + Proficiency if in Perception
    get passivePerception() {
        return 10 + this.statMod.statModifier[StatGroup.wisdom] + this.getProficiencyIfPerception();
    }

    private getProficiencyIfPerception(): number {
        return (this.hasPerceptionProficiency) ? this.statMod.proficiency : 0;
    }

    private get hasPerceptionProficiency(): boolean {
        return this.statMod.detailProficiency.indexOf(11) !== -1;
    }

}
