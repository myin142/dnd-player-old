import { Component } from '@angular/core';
import { StatsService } from '../../../services/stats.service';
import { BaseStats } from '../../../shared/stats.model';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-set-stats',
    templateUrl: './set-base-stats.component.html',
})
export class SetBaseStatsComponent {

    constructor(public stats: StatsService,
                private modal: ModalController) {
    }

    setBaseStats(stats: BaseStats): void {
        this.stats.saveBaseStats(stats);
        this.modal.dismiss();
    }

}
