import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SkillsService } from './skills.service';
import { moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-skills',
    templateUrl: './skills.page.html',
})
export class SkillsPage implements OnInit {

    formGroup: FormGroup;
    private init = false;

    constructor(private builder: FormBuilder,
                private skillService: SkillsService) {
    }

    ngOnInit() {
        this.formGroup = this.builder.group({
            slot1: [],
            slot2: [],
            slot3: [],
            slot4: [],
            cantrips: [[]],
            spells: [[]],
            prepared: [],
        });

        this.skillService.getSkills().subscribe(skills => {
            if (!this.init) {
                this.formGroup.setValue(skills);
                this.formGroup.valueChanges.subscribe(values => {
                    this.skillService.updateSkills(values);
                });
                this.init = true;
            }
        });
    }

    async reorderSpells(ev: any): Promise<void> {
        const value = this.spellsControl.value;
        const from = ev.detail.from;
        const to = ev.detail.to;
        console.log(ev.detail);
        console.log([ ...value ]);
        moveItemInArray(value, from, to);
        console.log([ ...value ]);

        this.spellsControl.setValue(value);
        await ev.detail.complete();
    }

    addSpell(spell: string) {
        const value = this.spellsControl.value;
        value.push(spell);
        this.spellsControl.setValue(value);
    }

    removeSpell(index: number): void {
        const value = this.spellsControl.value as [];
        value.splice(index, 1);
        this.spellsControl.setValue(value);
    }

    get spellsControl() {
        return this.formGroup.get('spells');
    }

    addCantrip(spell: string) {
        const value = this.cantripControl.value;
        value.push(spell);
        this.cantripControl.setValue(value);
    }

    removeCantrip(index: number): void {
        const value = this.cantripControl.value as [];
        value.splice(index, 1);
        this.cantripControl.setValue(value);
    }

    get cantripControl() {
        return this.formGroup.get('cantrips');
    }

    getIconForIndex(index: number): string {
        const prepared = this.formGroup.get('prepared').value;
        return (index < prepared) ? 'radio-button-on' : 'radio-button-off';
    }

}
