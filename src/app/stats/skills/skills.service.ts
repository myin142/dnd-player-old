import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from '../../services/storage.service';
import { StorageKey } from '../../shared/storage-key';
import { filter } from 'rxjs/operators';

@Injectable()
export class SkillsService {

    skills = new BehaviorSubject<Skills>(null);

    constructor(private storage: StorageService) {
        this.storage.get<Skills>(StorageKey.SKILLS).subscribe(skills => {
            this.skills.next(this.mapUndefinedToDefault(skills || {}));

            this.skills.subscribe(x => {
                this.storage.save(StorageKey.SKILLS, x).subscribe();
            });
        });
    }

    private mapUndefinedToDefault(skills: Partial<Skills>): Skills {
        return {
            slot1: skills.slot1 || 0,
            slot2: skills.slot2 || 0,
            slot3: skills.slot3 || 0,
            slot4: skills.slot4 || 0,
            cantrips: skills.cantrips || [],
            spells: skills.spells || [],
            prepared: skills.prepared || 0,
        };
    }

    getSkills(): Observable<Skills> {
        return this.skills.pipe(
            filter(x => x != null),
        );
    }

    updateSkills(skills: Partial<Skills>): void {
        this.skills.next({
            ...this.skills.value,
            ...skills,
        });
    }

}

export interface Skills {
    slot1: number;
    slot2: number;
    slot3: number;
    slot4: number;
    cantrips: string[];
    spells: string[];
    prepared: number; // First nth number of spells are prepared
}
