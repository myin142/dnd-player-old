import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillsPage } from './skills.page';
import { SkillsService } from './skills.service';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuModule } from '../../menu/menu.module';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
    declarations: [
        SkillsPage,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: '', component: SkillsPage},
        ]),
        IonicModule,
        MenuModule,
        ComponentsModule,
    ],
    providers: [
        SkillsService,
    ],
})
export class SkillsModule {
}
