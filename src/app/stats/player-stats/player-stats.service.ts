import { Injectable } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { PlayerStatSave } from './player-stats.model';
import { StorageKey } from '../../shared/storage-key';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable()
export class PlayerStatsService {

    playerStats = new BehaviorSubject<PlayerStatSave>(null);

    constructor(private storage: StorageService) {
        this.storage.get<PlayerStatSave>(StorageKey.PLAYER_STATS).subscribe(stats => {
            this.playerStats.next(this.mapUndefinedToDefault(stats || {}));

            this.playerStats.subscribe(x => {
                this.storage.save(StorageKey.PLAYER_STATS, x).subscribe();
            });
        });
    }

    private mapUndefinedToDefault(stats: Partial<PlayerStatSave>): PlayerStatSave {
        return {
            armor: stats.armor || 0,
            experience: stats.experience || 0,
            hitPoints: stats.hitPoints || 0,
            initiative: stats.initiative || 0,
            level: stats.level || 0,
            speed: stats.speed || 0,
            totalHitPoints: stats.totalHitPoints || 0,
        };
    }

    updateStats(stats: Partial<PlayerStatSave>): void {
        this.playerStats.next({
            ...this.playerStats.value,
            ...stats,
        });
    }

    getStats(): Observable<PlayerStatSave> {
        return this.playerStats.pipe(
            filter(x => x != null),
        );
    }

}
