import { Component, OnInit } from '@angular/core';
import { PlayerStatsService } from './player-stats.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-player-stats',
    templateUrl: './player-stats.page.html',
})
export class PlayerStatsPage implements OnInit {

    private init = false;
    formGroup: FormGroup;

    constructor(private playerStatsService: PlayerStatsService,
                private builder: FormBuilder) {
    }

    ngOnInit() {
        this.formGroup = this.builder.group({
            hitPoints: [],
            totalHitPoints: [],
            experience: [],
            armor: [],
            speed: [],
            level: [],
            initiative: [],
        });

        this.playerStatsService.getStats().subscribe(stats => {
            if (!this.init) {
                this.formGroup.setValue(stats);
                this.formGroup.valueChanges.subscribe(values => {
                    this.playerStatsService.updateStats(values);
                });
                this.init = true;
            }
        });


    }

}
