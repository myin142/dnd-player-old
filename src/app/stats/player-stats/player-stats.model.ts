export interface PlayerStatSave {
    hitPoints: number;
    totalHitPoints: number;
    experience: number;
    armor: number;
    speed: number;
    level: number;
    initiative: number;
}
