import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlayerStatsPage } from './player-stats.page';
import { MenuModule } from '../../menu/menu.module';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../../components/components.module';
import { PlayerStatsService } from './player-stats.service';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path: '', component: PlayerStatsPage},
        ]),
        MenuModule,
        IonicModule,
        ComponentsModule,
    ],
    declarations: [
        PlayerStatsPage,
    ],
    providers: [
        PlayerStatsService,
    ],
})
export class PlayerStatsModule {
}
