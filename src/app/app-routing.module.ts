import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'stats', pathMatch: 'full'},
    {path: 'stats', loadChildren: () => import('./stats/stats.module').then(m => m.StatsModule)},
    {path: 'inventory', loadChildren: () => import('./inventory/inventory.module').then(m => m.InventoryModule)},
    {path: 'notes', loadChildren: () => import('./notes/notes.module').then(m => m.NotesModule)},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
