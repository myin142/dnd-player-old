import { Injectable } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { BehaviorSubject } from 'rxjs';
import { StorageKey } from '../shared/storage-key';

@Injectable()
export class NotesService {

    notes = new BehaviorSubject<string>('');

    constructor(private storage: StorageService) {
        this.storage.get<string>(StorageKey.NOTES).subscribe(note => {
            this.notes.next(note);

            this.notes.subscribe(x => {
                this.storage.save(StorageKey.NOTES, x).subscribe();
            });
        });
    }

}
