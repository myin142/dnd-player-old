import { Component, OnInit } from '@angular/core';
import { NotesService } from './notes.service';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-notes',
    templateUrl: './notes.page.html',
    styles: [`
        textarea {
            resize: both;
        }
    `],
})
export class NotesPage implements OnInit {

    control = new FormControl();

    constructor(private noteService: NotesService) {
    }

    ngOnInit() {
        this.noteService.notes.subscribe(x => {
            this.control.setValue(x);
        });

        this.control.valueChanges.pipe(debounceTime(1000)).subscribe(x => {
            this.noteService.notes.next(x);
        });
    }

}
