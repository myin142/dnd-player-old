import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesPage } from './notes.page';
import { RouterModule } from '@angular/router';
import { NotesService } from './notes.service';
import { ComponentsModule } from '../components/components.module';
import { MenuModule } from '../menu/menu.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        NotesPage,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: '', component: NotesPage},
        ]),
        ComponentsModule,
        MenuModule,
        ReactiveFormsModule,
        IonicModule,
    ],
    providers: [
        NotesService,
    ],
})
export class NotesModule {
}
