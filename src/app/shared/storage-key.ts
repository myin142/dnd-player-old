export enum StorageKey {
    ABILITY_STATS = 'dnd-stats-key',
    PLAYER_STATS = 'dnd-player-stats-key',
    ITEM_BAG = 'dnd-item-bag-key',
    NOTES = 'dnd-notes-key',
    SKILLS = 'dnd-skills-key',
}
