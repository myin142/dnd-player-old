// TODO: should save it differently to easier handle the stats
// Saved to SQLite
export interface AbilityStats {
    baseStats: BaseStats;
    proficiency: number;
    baseProficiency: number[];
    detailProficiency: number[];
}

// For Calculating Detailed Stats
export interface StatModifier {
    statModifier: number[];
    proficiency: number;
    baseProficiency: number[];
    detailProficiency: number[];
}

// Keys has to match StatGroup enum values
export interface BaseStats {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
}

export enum StatGroup {
    strength,
    dexterity,
    constitution,
    intelligence,
    wisdom,
    charisma,
}

export interface Stat {
    name: string;
    statGroup: StatGroup;
}

// Names of stats should not be changed
export const baseStats: Stat[] = [
    { name: 'Strength', statGroup: StatGroup.strength },
    { name: 'Dexterity', statGroup: StatGroup.dexterity },
    { name: 'Constitution', statGroup: StatGroup.constitution },
    { name: 'Intelligence', statGroup: StatGroup.intelligence },
    { name: 'Wisdom', statGroup: StatGroup.wisdom },
    { name: 'Charisma', statGroup: StatGroup.charisma },
];
export const detailStats: Stat[] = [
    { name: 'Acrobatics', statGroup: StatGroup.dexterity },
    { name: 'Animal Handling', statGroup: StatGroup.wisdom },
    { name: 'Arcana', statGroup: StatGroup.intelligence },
    { name: 'Athletics', statGroup: StatGroup.strength },
    { name: 'Deception', statGroup: StatGroup.charisma },
    { name: 'History', statGroup: StatGroup.intelligence },
    { name: 'Insight', statGroup: StatGroup.wisdom },
    { name: 'Intimidation', statGroup: StatGroup.charisma },
    { name: 'Investigation', statGroup: StatGroup.intelligence },
    { name: 'Medicine', statGroup: StatGroup.wisdom },
    { name: 'Nature', statGroup: StatGroup.intelligence },
    { name: 'Perception', statGroup: StatGroup.wisdom },
    { name: 'Performance', statGroup: StatGroup.charisma },
    { name: 'Persuasion', statGroup: StatGroup.charisma },
    { name: 'Religion', statGroup: StatGroup.intelligence },
    { name: 'Sleight of Hand', statGroup: StatGroup.dexterity },
    { name: 'Stealth', statGroup: StatGroup.dexterity },
    { name: 'Survival', statGroup: StatGroup.wisdom },
];
