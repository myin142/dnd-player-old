import { Injectable } from '@angular/core';
import { AbilityStats, BaseStats, StatGroup, StatModifier } from '../shared/stats.model';
import { StorageKey } from '../shared/storage-key';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';
import { filter, map, pluck, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class StatsService {

    private abilityStats = new BehaviorSubject<AbilityStats>(null);

    constructor(private storage: StorageService) {
        this.storage.get<AbilityStats>(StorageKey.ABILITY_STATS).subscribe(stats => {
            this.abilityStats.next(this.mapUndefinedToDefault(stats || {}));

            this.abilityStats.subscribe(newStats => {
                this.storage.save(StorageKey.ABILITY_STATS, newStats).subscribe();
            });
        });
    }

    private mapUndefinedToDefault(stats: Partial<AbilityStats>): AbilityStats {
        return {
            baseStats: stats.baseStats,
            proficiency: stats.proficiency || 0,
            baseProficiency: stats.baseProficiency || [],
            detailProficiency: stats.detailProficiency || [],
        };
    }

    private updateAbilityStats(stats: Partial<AbilityStats>): void {
        this.abilityStats.next({
            ...this.abilityStats.value,
            ...stats,
        });
    }

    saveBaseStats(stats: BaseStats): void {
        this.updateAbilityStats({baseStats: stats});
    }

    getBaseStats(): Observable<BaseStats> {
        return this.abilityStats.pipe(
            filter(x => x != null),
            pluck('baseStats'),
        );
    }

    private statToNumberArray(stats: BaseStats): number[] {
        return Object.keys(StatGroup)
            .filter(k => typeof StatGroup[k] === 'number')
            .map(k => stats[k]);
    }

    getProficiency(): Observable<number> {
        return this.getStats().pipe(
            pluck('proficiency'),
        );
    }

    getStats(): Observable<AbilityStats> {
        return this.abilityStats.pipe(
            filter(x => x != null),
        );
    }

    getStatsModifier(): Observable<StatModifier> {
        return this.getStats().pipe(
            map(x => ({
                ...x,
                statModifier: this.statToNumberArray(x.baseStats).map(this.calculateModifier),
            })),
        );
    }

    private calculateModifier(num: number): number {
        return Math.floor((num - 10) / 2);
    }

    setProficiencyBonus(bonus: number): void {
        this.updateAbilityStats({proficiency: bonus});
    }

    addBaseProficiency(index: number): void {
        const state = this.toggleArrayValue(this.abilityStats.value.baseProficiency, index);
        this.updateAbilityStats({baseProficiency: state});
    }

    addDetailProficiency(index: number): void {
        const state = this.toggleArrayValue(this.abilityStats.value.detailProficiency, index);
        this.updateAbilityStats({detailProficiency: state});
    }

    private toggleArrayValue<T>(arr: T[], item: T): T[] {
        let changedArray = [ ...arr ];

        if (arr.indexOf(item) === -1) {
            changedArray.push(item);
        } else {
            changedArray = changedArray.filter(x => x !== item);
        }

        return changedArray;
    }
}


