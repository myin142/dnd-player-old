import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { from, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class StorageService {

    constructor(private storage: Storage) {
    }

    save<T>(key: string, value: T): Observable<void> {
        return from(this.storage.set(key, value));
    }

    get<T>(key: string): Observable<T> {
        return from(this.storage.get(key));
    }

}
