import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-menu-close-header',
    templateUrl: './menu-close-header.component.html',
})
export class MenuCloseHeaderComponent {
    @Input() title: string;

    constructor(public modal: ModalController) {}
}
