import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuBackHeaderComponent } from './menu-back-header.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavController } from '@ionic/angular';

describe('MenuBackHeaderComponent', () => {

    let fixture: ComponentFixture<MenuBackHeaderComponent>;
    let component: MenuBackHeaderComponent;

    let nav: NavController;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([])],
            declarations: [MenuBackHeaderComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(MenuBackHeaderComponent);
        component = fixture.componentInstance;

        nav = TestBed.get(NavController);
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain title', () => {
        const title = 'Title';
        component.title = title;
        fixture.detectChanges();

        const elem = fixture.nativeElement.querySelector('ion-title');
        expect(elem.textContent).toEqual(title);
    });

    it('should have button with arrow-back icon', () => {
        const icon = fixture.nativeElement.querySelector('ion-button ion-icon');
        expect(icon.getAttribute('name')).toEqual('arrow-back');
    });

    it('should navigate back on button click', () => {
        spyOn(nav, 'back');

        const button = fixture.nativeElement.querySelector('ion-button');
        button.click();

        expect(nav.back).toHaveBeenCalled();
    });

});
