import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-menu-back-header',
    templateUrl: './menu-back-header.component.html',
})
export class MenuBackHeaderComponent {
    @Input() title: string;

    constructor(public nav: NavController) {}
}
