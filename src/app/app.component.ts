import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuItem } from './menu/menu-list/menu-item.model';
import { build } from '../environments/build-info';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {

    public build = build;
    public appPages: MenuItem[] = [
        {
            title: 'Stats',
            url: '/stats',
            icon: 'stats',
        },
        {
            title: 'Inventory',
            url: '/inventory',
            icon: 'basket',
        },
        {
            title: 'Notes',
            url: '/notes',
            icon: 'clipboard',
        },
    ];

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
        });
    }
}
