#!/bin/bash -v

set -e

if [[ "$TRAVIS_BRANCH" == "develop" ]]
then
	echo "Skipping Deployment on Develop Branch"
    exit
fi

mkdir -p output
cp platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk output/android-release-unsigned.apk

